
# 基于microsoft/dotnet:latest构建Docker Image  
FROM microsoft/aspnetcore:2.0  
 
# 设置工作路径  
WORKDIR /build  
   
# 拷贝文件  
COPY . .  
 
# 向外界暴露5000端口  
EXPOSE 5000  
   
# 执行dotnet TestCore.dll命令  
ENTRYPOINT ["dotnet", "dockerApiDemo.dll"]
